import { BASE_URL } from './type';
import axios from 'axios'
export function useApi() {
    async function POST(url, params) {
        const result = await axios({ method: 'POST', url: `${BASE_URL}${url}`, data: params })
        return result
    }
    async function GET(url, params) {
        const result = await axios({ method: 'GET', url: `${BASE_URL}${url}`, params })
        return result
    }

    return {
        async getShows() {
            const { data } = await GET('/shows');
            return data
        },
        async getShow(id) {
            const { data } = await GET('/shows/' + id + '?embed=cast');
            return data
        },
        async getEpisodes(id) {
            const { data } = await GET('/shows/' + id + '/episodes');
            return data
        },
        async getSearch(id) {
            const { data } = await GET('/search/shows?q=' + id);
            return data
        },
    }
}