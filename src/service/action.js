import { useApi } from './api';

export function getShows() {
    return async () => {
        const api = useApi();
        try {
            const info = await api.getShows();
            return info
        } catch (e) {
            console.log('+ error', e)
        }

    }
}
export function getShow() {
    return async (param) => {
        const api = useApi();
        try {
            const info = await api.getShow(param);
            return info
        } catch (e) {
            console.log('+ error', e)
        }

    }
}
export function getSearch() {
    return async (param) => {
        const api = useApi();
        try {
            const info = await api.getSearch(param);
            return info
        } catch (e) {
            console.log('+ error', e)
        }

    }
}
export function getEpisodes() {
    return async (param) => {
        const api = useApi();
        try {
            const info = await api.getEpisodes(param);
            return info
        } catch (e) {
            console.log('+ error', e)
        }

    }
}