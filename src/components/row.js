const Row = ({ data, handleView, handleFavorite, favorite }) => (
    data && data.image ?
        <div className="data-item">
            <div style={{ position: 'relative' }}>
                <img src={data.image.medium} />
                {data.status === 'airing' && <div className="airing">Airing</div>}
            </div>

            <div className="name">{data.name}, {data.premiered}
                <div>
                    {data.genres.map(g => (<span key={g}>{g}, </span>))}
                </div>
            </div>
            <div style={{
                color: data.rating.average > 7 ?
                    'green' : data.rating.average > 3 ?
                        'orange' : 'red'
            }}
            >
                {data.rating.average}
            </div>
            <div className="buttons">
                <button className={favorite ? 'favorite' : ''} onClick={handleFavorite}>{favorite ? 'Unfavorite' : 'Favourite'}</button>
                <button className="detail-btn" onClick={handleView}>View Detail</button>
            </div>

        </div> : null
);

export default Row