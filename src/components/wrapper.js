import React from 'react';
import Loading from "react-loading";

const SiteWrapper = (props) => (
    <div>
        {props.loading && (
            <div className='loading'>
                <Loading type='spinningBubbles' color='#000000' />
            </div>
        )}
        <div className='App'>
            {props.children}
        </div>
    </div>
)

export default SiteWrapper;