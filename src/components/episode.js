const Episode = ({ title, data }) => (
    <div style={{ marginTop: 15 }}>
        <div style={{
            textAlign: 'center',
            fontWeight: 'bold',
            marginBottom: 15
        }}
        >
            Season {title}
        </div>
        {
            data.map(d => (
                <div key={d.id}>Episode {d.id} - {d.name}</div>
            ))
        }
    </div>
)

export default Episode;