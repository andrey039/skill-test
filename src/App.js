import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getShows, getSearch } from './service/action';
import Row from './components/row'
import './App.css';
import SiteWrapper from './components/wrapper';

function App() {
  const [data, setData] = useState([]);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [favorite, setFavorite] = useState([]);
  const navigate = useNavigate();
  const getlist = getShows();
  const searchapi = getSearch();
  useEffect(() => {
    getInitalData();
    let jsonData = localStorage.getItem('favorite');
    if (jsonData) {
      let array = JSON.parse(jsonData);
      setFavorite(array);
    }
  }, []);
  const getInitalData = async () => {
    setLoading(true);
    const result = await getlist();
    setData(result);
    setLoading(false);
  }
  const handleView = (id) => navigate('/detail/' + id);
  const handleChange = e => {
    setSearch(e.target.value);
  }
  useEffect(() => {
    handleSearch();
  }, [search]);
  const handleSearch = async () => {
    if (!search) {
      getInitalData();
      return;
    }
    setLoading(true);
    const result = await searchapi(search);
    let array = [];
    if (typeof (result) === 'object')
      result.forEach(r => {
        array.push(r.show);
      });

    setData(array)
    setLoading(false);
  }
  const handleFavorite = (id) => {
    let favorites = []
    let f = localStorage.getItem('favorite');
    if (f) {
      favorites = JSON.parse(f);
      let index = favorites.indexOf(id);
      if (index > -1) {
        favorites.splice(index, 1);
      } else {
        favorites.push(id);
      }
      localStorage.setItem('favorite', JSON.stringify(favorites));
      setFavorite(favorites)
    } else {
      localStorage.setItem('favorite', JSON.stringify([id]));
      setFavorite([id]);
    }
  }
  return (
    <SiteWrapper loading={loading}>
      <div style={{ textAlign: 'center' }}>
        <input
          placeholder='Search Input'
          type={'text'}
          value={search}
          onChange={handleChange}
          style={{ marginBottom: 20 }}
        />
        {
          data.map(d => (<Row
            data={d}
            key={d.id}
            handleView={() => handleView(d.id)}
            handleFavorite={() => handleFavorite(d.id)}
            favorite={favorite.indexOf(d.id) > -1}
          />))
        }
      </div>
    </SiteWrapper>
  );
}

export default App;
