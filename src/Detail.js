import { useEffect, useState } from 'react';
import { getShow, getEpisodes } from './service/action';
import { useParams } from 'react-router-dom'
import _ from 'lodash';
import './App.css';
import SiteWrapper from './components/wrapper';
import Episode from './components/episode';

function Detail() {
    let { id } = useParams();
    const [data, setData] = useState(null);
    const [episodes, setEpisodes] = useState(null);
    const [favorite, setFavorite] = useState([]);
    const [loading, setLoading] = useState(false);
    const getdetail = getShow();
    const getepisodes = getEpisodes();
    useEffect(() => {
        getInitalData();
        let jsonData = localStorage.getItem('favorite');
        if (jsonData) {
            let array = JSON.parse(jsonData);
            setFavorite(array);
        }
    }, []);
    const getInitalData = async () => {
        setLoading(true);
        const result = await getdetail(id);
        const result1 = await getepisodes(id);
        setData(result);
        setEpisodes(groupBy(result1, 'season'));
        setLoading(false);
    }
    const groupBy = (array, key) => {
        return _.mapValues(_.groupBy(array, key), list => list.map(l => _.omit(l, key)));
    }
    const handleFavorite = () => {
        let id = data.id;
        let favorites = []
        let f = localStorage.getItem('favorite');
        if (f) {
            favorites = JSON.parse(f);
            let index = favorites.indexOf(id);
            if (index > -1) {
                favorites.splice(index, 1);
            } else {
                favorites.push(id);
            }
            localStorage.setItem('favorite', JSON.stringify(favorites));
            setFavorite(favorites)
        } else {
            localStorage.setItem('favorite', JSON.stringify([id]));
            setFavorite([id]);
        }
    }

    return (
        <SiteWrapper loading={loading}>
            {data &&
                <div className='detail'>
                    <img id='img' src={data.image.original} />
                    <div className='info'>
                        <div id='name-view'>
                            <div>{data.name}</div>
                            <button
                                className={favorite.indexOf(data.id) > -1 ? 'favorite' : ''}
                                onClick={handleFavorite}
                                style={{ cursor: 'pointer' }}
                            >
                                {favorite.indexOf(data.id) > -1 ? 'Unfavourite' : 'Favourite'}
                            </button>
                        </div>
                        <div id='des-view'>
                            <div style={{
                                color: data.rating.average > 7 ?
                                    'green' : data.rating.average > 3 ?
                                        'orange' : 'red'
                            }}
                            >
                                <span style={{ color: '#000' }}>Rating:</span> {data.rating.average}
                            </div>
                            <div>Premiered: {data.premiered}</div>
                            <div>Status: {data.status}</div>
                            <div>
                                Genres: {data.genres.map(g => (<span key={g}>{g}, </span>))}
                            </div>
                            <span dangerouslySetInnerHTML={{ __html: data.summary }}></span>
                        </div>
                        <div className='episodes-list'>
                            {episodes &&
                                Object.keys(episodes).map(e => <Episode key={e} title={e} data={episodes[e]} />)
                            }
                        </div>
                    </div>
                </div>
            }
        </SiteWrapper>
    );
}

export default Detail;
